# Flurry AAVE Project

This project demonstrates a basic AAVE protocol use case.

deployed contract on Kovan:0xF7987A1F9a56798BE9eE822e2DDFa5631d46443d

View on Etherscan:https://kovan.etherscan.io/address/0xF7987A1F9a56798BE9eE822e2DDFa5631d46443d#code

To start:
```
npm install
cp .env.dist .env
```
enter your metamask key

To test:
```
npx hardhat clean
npx hardhat compile
npx hardhat test --network kovan
```
Please noted that for the testing, I used USDT on kovan to test, so make sure the account has at least 100 USDT
