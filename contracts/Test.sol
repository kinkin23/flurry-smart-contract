// SPDX-License-Identifier: MIT
pragma solidity 0.6.12;

import {ILendingPool} from "@aave/protocol-v2/contracts/interfaces/ILendingPool.sol";
import {ILendingPoolAddressesProvider} from "@aave/protocol-v2/contracts/interfaces/ILendingPoolAddressesProvider.sol";
import {IAToken} from "@aave/protocol-v2/contracts/interfaces/IAToken.sol";
import {AaveProtocolDataProvider} from "@aave/protocol-v2/contracts/misc/AaveProtocolDataProvider.sol";
import {IERC20} from "@aave/protocol-v2/contracts/dependencies/openzeppelin/contracts/IERC20.sol";



interface MyContract {

    /// @dev Deposit ERC20 tokens on behalf of msg.sender to Aave Protocol
    /// @param _erc20Contract The address fo the underlying asset to deposit to Aave Protocol v2
    /// @param _amount The amount of the underlying asset to deposit
    /// @return success Whether the deposit operation was successful or not
    function deposit(address _erc20Contract, uint256 _amount) external returns (bool success);

    /// @dev Withdraw ERC20 tokens on behalf of msg.sender from Aave Protocol
    /// @param _erc20Contract The address of the underlyng asset being withdrawn
    /// @param _amount The amount to be withdrawn
    /// @return amountWithdrawn The actual amount withdrawn from Aave
    function withdraw(address _erc20Contract, uint256 _amount) external returns (uint256 amountWithdrawn);

    /// @dev Read only function
    /// @return amountInEth Returns the value locked as collateral posted by msg.sender
    function checkCollateralValueInEth() external view returns (uint256 amountInEth);
}

contract Test is MyContract{

    AaveProtocolDataProvider dataProvider = AaveProtocolDataProvider(0x3c73A5E5785cAC854D468F727c606C07488a29D6);
    ILendingPoolAddressesProvider iLendingPoolAddress = ILendingPoolAddressesProvider(0x88757f2f99175387aB4C6a4b3067c77A695b0349);
    address lendingPoolAddress = iLendingPoolAddress.getLendingPool();
    ILendingPool iLendingPool = ILendingPool(lendingPoolAddress);

    function deposit(address _erc20Contract,uint256 _amount) override public returns(bool){
        IERC20(_erc20Contract).transferFrom(msg.sender, address(this), _amount);
        bool result = IERC20(_erc20Contract).approve(lendingPoolAddress,_amount);
        iLendingPool.deposit(_erc20Contract, _amount, msg.sender,0);
        return result;
    }

    function withdraw(address _erc20Contract, uint256 _amount) override public returns(uint256){
        (address aTokenAddress,,) = dataProvider.getReserveTokensAddresses(_erc20Contract);
        IAToken aToken = IAToken(aTokenAddress);
        aToken.transferFrom(msg.sender,address(this), _amount);
        uint256 amountWithdrawn = iLendingPool.withdraw(_erc20Contract, _amount, msg.sender);
        return amountWithdrawn;
    }

    function checkCollateralValueInEth() override public view returns(uint256){
        (uint256 result, , , , , ) = iLendingPool.getUserAccountData(msg.sender);
        return result;
    }

    function checkBalance(address user,address _erc) public view returns(uint256){
        (address aTokenAddress,,) = dataProvider.getReserveTokensAddresses(_erc);
        uint256 assetBalance = IERC20(aTokenAddress).balanceOf(user);
        return assetBalance;
    }

    function checkAddress(address _erc) public view returns(address){
        (address aTokenAddress,,) = dataProvider.getReserveTokensAddresses(_erc);
        return aTokenAddress;
    }


}
