require("@nomiclabs/hardhat-waffle");
require("@nomiclabs/hardhat-etherscan");
require('dotenv').config()

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

const METAMASK_PRIVATE_KEY = process.env.METAMASK_SECRET_KEY;

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: "0.6.12",
  settings: {
    optimizer: {
      enabled: true,
      runs: 200
    }
  },
  networks: {
    kovan: {
      url: 'https://kovan.infura.io/v3/'+process.env.KOVAN_PRIVATE_KEY,
      accounts: [`${METAMASK_PRIVATE_KEY}`]
      // accounts: { mnemonic: mnemonic },
    }
  },
  mocha: {
    timeout: 100000
  }
};
