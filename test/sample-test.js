const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Test", function () {
    let test;
    let myContract;
    beforeEach(async () => {
        const Test = await ethers.getContractFactory('Test');
        test = await Test.deploy();
        await test.deployed();
        myContract = await ethers.getContractAt("MyContract", test.address);
    });
  it('deposit returns value > 0', async function () {
      const usdtContractAddress = '0x13512979ADE267AB5100878E2e0f485B568328a4';
      const token = await ethers.getContractAt("IERC20", usdtContractAddress);
      // deposit 100 usdt
      await token.approve(test.address,100000000);
      await myContract.deposit(usdtContractAddress,100000000);
      const result = await myContract.checkCollateralValueInEth();
      expect(parseInt(result.toString())).to.be.above(0);
  });
  it('withdraw returns value = 0', async function () {
        const usdtContractAddress = '0x13512979ADE267AB5100878E2e0f485B568328a4';
        const [deployer] = await ethers.getSigners();
        const address = await deployer.getAddress();
        const aTokenAddress = await test.checkAddress(usdtContractAddress);
        const aToken = await ethers.getContractAt("IERC20", aTokenAddress);
        // withdraw all the balance
        const balance = await test.checkBalance(address,usdtContractAddress);
        await aToken.approve(test.address,balance.toString());
        await myContract.withdraw(usdtContractAddress,balance.toString());
        const result = await myContract.checkCollateralValueInEth();
        expect(result.toString()).to.equal('0');
  });
});
